**The Expense Reimbursement System**

**Project Descrtiption**

The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending.
Reimbursement Types Employees must select the type of reimbursement as: LODGING, TRAVEL, FOOD, or OTHER.

**Technologies Used**

-Frontend : Html, CSS, Javascript

-Backend : Java, Maven, Servlet, JDBC, Tomcat

-Database : PostgreSQL


**Features**

-The application employs the DAO design pattern, and separate the code into the appropriate layers

-The application is deployed on Tomcat server.

-Employee can log into the system.

-View all submitted request in the past.

-Create new reimbursment request.

-Finance Manager can log in into the system.

-View All submitted request.

-Finance Manager can filter request based on status :Approved, Pending or Denied.

-Finance Manager can Approve or reject the request


**ToDo List:**

-Users can upload an image of their receipt when submitting reimbursement requests.

-Java mail feture to send temperory password

-Password should be encrypted
